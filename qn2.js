
// Declared the different auctions and initilised array of bidders and their bids.
const auction_1 = [1, "A", 5, "B", 10, "A", 8, "A", 17, "B", 17];
const auction_2 = [
  1,
  "Henry",
  15,
  "Mukasa",
  24,
  "Barbra",
  30,
  "Asiimwe",
  31,
  "Mark",
  49,
  "Tim",
  57,
  "Frank",
  59,
  "Hellen",
  61,
  "Atim",
  64,
  "Mukasa",
  74,
  "Mukasa",
  69,
  "Mukasa",
  71,
  "Hellen",
  78,
  "Frank",
  78,
  "Barbra",
  95,
  "Frank",
  103,
  "Asiimwe",
  135,
];
const auction_3 = [
  1,
  "Mathius",
  5,
  "Ruth",
  10,
  "Carl",
  19,
  "Mathius",
  14,
  "Mathius",
  23,
  "Carl",
  24,
  "Carl",
  25,
  "Ruth",
  26,
];
const auction_4 = [
  1,
  "Lutaya",
  5,
  "Otim",
  10,
  "Sheila",
  19,
  "Lutaya",
  23,
  "Sheila",
  24,
  "Lutaya",
  29,
  "Otim",
  26,
];

function auction(ar) {
    // Removing the initial bid $1 using splice method. 
  ar.splice(0, 1); 
  let array_bidders = [];

  for (let i = 0; i < ar.length; i++) {
    if (typeof ar[i] == "string") {
      array_bidders.push(ar[i]);
    }
  }

  let array_bid = [];
  for (let i = 0; i < ar.length; i++) {
    if (typeof ar[i] !== "string") {
      array_bid.push(ar[i]);
    }
  }

  // Finding maximun bid amount using inbuilt function math.max to pick from bids removed from array using spreader operator.
  let max_bid = Math.max(...array_bid);
  console.log("Maximum bid",max_bid)
  // Find index of maximum bid using inbuilt function indexOf
  let position_bid = array_bid.indexOf(max_bid);
  console.log("Position of maximum bid",position_bid)

  let name_of_bidder = array_bidders[position_bid];
  console.log("Name of the bidders", name_of_bidder)
  // Declear array highest bidder to print results.

  const highest_bidder = [name_of_bidder, max_bid];

  // console.log(array_bidders)
  // console.log(array_bid)
  // console.log(max_bid)
  // console.log(position_bid)
  // console.log(name_of_bidder)
  console.log(highest_bidder);
}

// Calling  function for auction 1
auction(auction_1);
// Calling  function for auction 2
auction(auction_2);

// Calling  function for auction 3
auction(auction_3);

// Calling  function for auction 4
auction(auction_4);
